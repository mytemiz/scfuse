#!/usr/bin/env python3

import json
import soundcloud
import urllib
import sys

import config as config

TRACK_LIMIT = 25

class SCApi:
    def __init__(self, client_id=config.CLIENT_ID, access_token=config.ACCESS_TOKEN):
        self.client_id = client_id
        self.access_token = access_token
        
        self.getClient()

    def getClient(self):
        if self.access_token:    
            self.client = soundcloud.Client(access_token=self.access_token)
        else:
            raise Exception('Access token not found!')

    def getTrackInfo(self, url):
        if self.client:
            track = self.client.get('/resolve', url=track_url)
            return track

    def followings(self):
        if self.client:
            followings = self.client.get('/me/followings')
            collection = followings.collection
            return collection
    
    def followers(self):
        if self.client:
            followers = self.client.get('/me/followers')
            collection = followers.collection
            return collection
  
    def groups(self):
        if self.client:
            groups = self.client.get('/me/groups')
            return  groups

    def likes(self):
        '''
        :returns: list of liked tracks
        :rtype: list
        '''
        if self.client:
            favorites = self.client.get('/me/favorites')
            return favorites

    def playlists(self):
        if self.client:
            playlists = self.client.get('/me/playlists')
            return playlists

    def tracks(self):
        if self.client:
            tracks = self.client.get('/me/tracks')
            return tracks

    def searchTracks(self, search_key, limit=TRACK_LIMIT):
        if self.client:
            tracks = self.client.get('/tracks', q=search_key, limit=limit)
            collection = tracks.obj['collection']
            return collection

    def createPlaylist(self, title, sharing='public', tracks=[]):
        if self.client:
            self.client.post('/playlists', playlist={
                'title' : title,
                'sharing' : sharing,
                'tracks' : tracks
                })
            print('playlist created:', title)

    def followUser(self, userid):
        if self.client:
            self.client.put('/me/followings/%s' % userid)

    def unfollowUser(self, userid):
        if self.client:
            self.client.delete('/me/followings/%s' % userid)

    def likeTrack(self, trackid):
        if self.client:
            self.client.put('/me/favorites/%s' % trackid)

    def unlikeTrack(self, trackid):
        if self.client:
            self.client.delete('/me/favorites/%s' % trackid)

    def uploadAudio(self, databuffer, title=None):
        databuffer.seek(0)
        if self.client:
            if title is None:
                title = "test title"
            track = self.client.post('/tracks', track={
                'title': title,
                'asset_data': databuffer
                })

'''
Helper functions
'''

# resolve_url must be formatted with search_key and client_id values before use
resolve_url = 'http://api.soundcloud.com/resolve?url=http://soundcloud.com/{search_key}&client_id={client_id}'

def getUser(username):
    '''
    retrieve user object from username
    
    :returns: user dictionary
    :rtype: dict
    '''
    url = resolve_url.format(search_key=username, client_id=config.client_id) 
    with urllib.request.urlopen(url) as response:
        html = response.read().decode('utf-8')

    user = json.loads(html)
    return user

