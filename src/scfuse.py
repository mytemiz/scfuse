#!/usr/bin/env python3

# Fuse implemantation for simple read only SoundCloud file system.

import os
import sys
import errno
import stat
import io

from src.lib.fuse import FUSE, FuseOSError, Operations
#from fuse import FUSE, FuseOSError, Operations

from src.scapi import SCApi

'''
!! Only myprofile part works

TODO:
file tree:
  root(/)
    \_ explore _
    |           \_ users
    |           \_ tracks
    |           \_ playlists
    |           \_ ...
    |
    |
    |
    |
    \_ myprofile _
                  \_ followers
                  |_ likes
                  |_ playlist _
                               \_ private
                               \_ public
                  |_ following
                  |_ groups
                  \_ tracks _
                             \_ private
                             \_ public
'''

mainfolders = [
        'followers', 
        'likes', 
        'playlists', 
        'followings', 
        'groups', 
        'tracks'
    ]

subfolders = [
        'playlists/private',
        'playlists/public',
        'tracks/private',
        'tracks/public'
        ]

class Stat:
    def __init__(self):
        self.st_mode= 0
        self.t_ino= 0
        self.st_dev= 0
        self.st_nlink= 0
        self.st_uid= os.getuid()
        self.st_gid= os.getgid()
        self.st_size= 0
        self.st_blksize= 0
        self.st_atime= 0
        self.st_mtime= 0
        self.st_ctime= 0

class ReadOnlyFuse(Operations):
    def __init__(self, root):
        # after successfully calling this function
        # use self.scon to reach api funcs
        self.root = root
        self.scon = self.connectSC()
        self.filelist = set()
        self.fd = io.BytesIO()
        
        print('Operations bound successfully.')

    # Helpers
    # =======

    def connectSC(self):
        try:
            scon = SCApi()
            return scon
        except:
            print('ERROR -', sys.exc_info()[1])
            print('Cannot connect to soundcloud api.')
            sys.exit(-1)

    def _full_path(self, partial):
        if partial.startswith("/"):
            partial = partial[1:]
        path = os.path.join(self.root, partial)
        print(path)
        return path
    # Filesystem methods
    # ==================
    
    def access(self, path, mode):
        print('access\n')
        #full_path = self._full_path(path)
        #if not os.access(path, mode):
        #    raise FuseOSError(errno.EACCES)
    
    def chmod(self, path, mode):
        print('chmod\n')
        full_path = self._full_path(path)
        return os.chmod(full_path, mode)

    def chown(self, path, uid, gid):
        print('chown\n')
        full_path = self._full_path(path)
        return os.chown(full_path, uid, gid)

    def getattr(self, path, fh=None):
        print('getattr - path: ', path)
        st = Stat()


        if path == '/':
            st.st_mode = stat.S_IFDIR | 0o755
            st.st_nlink = 2
        else:
            print(self.filelist)
            if path.lstrip('/') in mainfolders:
                st.st_mode = stat.S_IFDIR | 0o755
                st.st_nlink = 2
                #elif path == '/linkfollowings':
                #    st.st_mode = stat.S_IFLNK | 0o755
                #    st.st_nlink = 1
            elif path.lstrip('/') in subfolders:
                st.st_mode = stat.S_IFDIR | 0o755
                st.st_nlink = 2
            else:
                if path not in self.filelist:
                    return os.stat(path)
                #print(os.stat(path))
                st.st_mode = stat.S_IFREG | 0o444
                st.st_nlink = 1
        
        full_path = self._full_path(path)
        #st = os.lstat(full_path)
        retdict = dict((key, getattr(st, key)) for key in ('st_atime', 'st_ctime',
                     'st_gid', 'st_mode', 'st_mtime', 'st_nlink', 'st_size', 'st_uid'))
        print("----------------\n")
        return retdict

    def readdir(self, path, fh):
        print('readdir\n')
        #full_path = self._full_path(path)
        self.filelist.clear()

        dirents = ['.', '..']
        if path == '/':
            for key in mainfolders:
                dirents.append(key)
            #dirents.append('linkfollowings')

        if path == '/followers':
            followers = self.scon.followers()
            for user in followers:
                fname = user.username + '_' + str(user.id)
                dirents.append(fname)
        elif path == '/followings':
            followings = self.scon.followings()
            for user in followings:
                fname = user.username + '_' + str(user.id)
                dirents.append(fname)
        elif path == '/groups':
            groups = self.scon.groups()
            for group in groups:
                fname = group.name + '_' + str(group.id)
                dirents.append(fname)
        elif path == '/likes':
            likes = self.scon.likes()
            for track in likes:
                fname = track.title + '_' + str(track.id)
                dirents.append(fname)
        elif path == '/playlists':
            dirents.extend(['public','private'])
        elif path == '/playlists/public':
            ## each playlist directory should  show own tracks
            playlists = self.scon.playlists()
            for playlist in playlists:
                if playlist.sharing == 'public':
                    fname = playlist.title + '_' + str(playlist.id)
                    dirents.append(fname)
        elif path == '/playlists/private':
            ## each playlist directory should show own tracks
            playlists = self.scon.playlists()
            for playlist in playlists:
                if playlist.sharing == 'private':
                    fname = playlist.title + '_' + str(playlist.id)
                    dirents.append(fname)
        elif path == '/tracks':
            dirents.extend(['public','private'])
        elif path == '/tracks/public':
            tracks = self.scon.tracks()
            for track in tracks:
                if track.sharing == 'public':
                    fname = track.title + '_' + str(track.id) + '.mp3'
                    dirents.append(fname)
        elif path == '/tracks/private':
            tracks = self.scon.tracks()
            for track in tracks:
                if track.sharing == 'private':
                    fname = track.title + '_' + str(track.id) + '.mp3'
                    dirents.append(fname)
    
        #if os.path.isdir(full_path):
        #    dirents.extend(os.listdir(full_path))
        for r in dirents:
            self.filelist.add(os.path.join(path, r))
            yield r

    def readlink(self, path):
        print('readlink\n')
        #pathname = os.readlink(self._full_path(path))
        pathname = path
        #if path == '/linkfollowings':
        #    return 'followings'
        if pathname.startswith("/"):
            # Path name is absolute, sanitize it.
            return os.path.relpath(pathname, self.root)
        else:
            return pathname

    def mknod(self, path, mode, dev):
        print('mknod\n')
        raise FuseOSError(errno.EROFS)

    def rmdir(self, path):
        print('rmdir\n')
        raise FuseOSError(errno.EROFS)

    def mkdir(self, path, mode):
        print('mkdir - path: %s  mode: %s\n' %(path, mode))
        new_path = os.path.join(os.getcwd(), path.lstrip('/'))
        print(new_path)
        self.filelist.add(path)
        print(self.filelist)
        res = os.mkdir(new_path, mode)
        self.filelist.add(path)
        return res
        #raise FuseOSError(errno.EROFS)

    def statfs(self, path):
        print('statfs\n')
        #full_path = self._full_path(path)
        #stv = os.statvfs(full_path)
        st = Stat()

        return  dict((key, getattr(st, key)) for key in ('st_atime', 'st_ctime',
                     'st_gid', 'st_mode', 'st_mtime', 'st_nlink', 'st_size', 'st_uid'))
        '''
        return dict((key, getattr(st, key)) for key in ('f_bavail', 'f_bfree',
            'f_blocks', 'f_bsize', 'f_favail', 'f_ffree', 'f_files', 'f_flag',
            'f_frsize', 'f_namemax'))
        '''
    def unlink(self, path):
        print('unlink\n')
        if path in self.filelist:
            if os.path.dirname(path) == '/followings':
                self.filelist.remove(path)
                self.scon.unfollowUser(path.split('_')[-1])
                return 0
            elif os.path.dirname(path) == '/likes':
                self.filelist.remove(path)
                self.scon.unlikeTrack(path.split('_')[-1])
                return 0
            else:
                raise FuseOSError(errno.EROFS)

        raise FuseOSError(errno.ENOENT)

    def symlink(self, name, target):
        print('symlink\n')
        raise FuseOSError(errno.EROFS)

    def rename(self, old, new):
        print('rename\n')
        raise FuseOSError(errno.EROFS)

    def link(self, target, name):
        print('link\n')
        raise FuseOSError(errno.EROFS)

    def utimens(self, path, times=None):
        print('utimens path:%s\n' % path)
        new_path = os.path.join(os.getcwd(), path.lstrip('/'))
        print(new_path)
        #print(os.utime(new_path, times))
        return 0
        #raise FuseOSError(errno.EROFS)

    # File methods
    # ============
    
    def open(self, path, flags):
        print('open - path %s\n' % path)
        # allow opening bu not allow editing
        
        # raise file manager cache files requests
        if path == '/.directory' or path == '/.hidden':
            raise FuseOSError(errno.EACCES)
        #if (flags & os.O_WRONLY) or (flags & os.O_RDWR) or (flags & os.O_CREAT) or (flags & os.O_EXCL) or (flags & os.O_TRUNC) or (flags & os.O_APPEND): 
        #    raise FuseOSError(errno.EROFS)
        full_path = self._full_path(path)
        print("fullpath: ",full_path)
        #print(os.open(full_path, flags))
        #return os.open(full_path, flags)
        return 0

    def create(self, path, mode, fi=None):
        print('create path: %s  mode: %s  fi: %s\n' % (path, mode, fi))
        new_path = os.path.join(os.getcwd(), path.lstrip('/'))
        
        if os.path.dirname(path) == '/followings':
            try:
                self.scon.followUser(os.path.basename(path))
                self.filelist.add(path)
                res = 0
            except:
                raise FuseOSError(errno.EROFS)
        elif os.path.dirname(path) == '/likes':
            try:
                self.scon.likeTrack(os.path.basename(path))
                self.filelist.add(path)
                res = 0
            except:
                raise FuseOSError(errno.EROFS)
        elif os.path.dirname(path) == '/tracks/public':
            #raise FuseOSError(errno.EPERM)
            self.filelist.add(path)
            res = 0
        elif os.path.dirname(path) == '/tracks/private':
            self.filelist.add(path)
            res = 0
        else:
            raise FuseOSError(errno.EROFS)
        return res

    def read(self, path, length, offset, fh):
        print('read - path %s\n' % path)
        print('length: ', length, " offset: ", offset, " fh: ", fh)
        #os.lseek(fh, offset, os.SEEK_SET)
        
        #return os.read(fh, length)
        return 0

    def write(self, path, buf, offset, fh):
        print('write - path: %s - fd: %d\n' % (path, fh))
        #r = -1
        if os.path.dirname(path) == '/tracks/public' or os.path.dirname(path) == '/tracks/private':
            if (self.fd.closed):
                self.fd = io.BytesIO()

            r = self.fd.write(buf)
            return r
        raise FuseOSError(errno.EROFS)
        #return r

    def truncate(self, path, length, fh=None):
        print('truncate - path: %s\n' % path)
        raise FuseOSError(errno.EROFS)

    def flush(self, path, fh):
        print('flush - path: %s\n' % path)
        if os.path.dirname(path) == '/tracks/public' or os.path.dirname(path) == '/tracks/private':
            filename = os.path.basename(path)
            title = os.path.splitext(filename)[0]
            self.scon.uploadAudio(self.fd, title=title)
            self.metadata = {}
            self.fd.close()
        return 0

    def release(self, path, fh):
        print('release - path: %s - fh:%d \n' % (path, fh))
        return 0

    def fsync(self, path, fdatasync, fh):
        print('fsync - path: %s\n' % path)
        return 0

def main(mountpoint):
    print('Mounting...')
    FUSE(ReadOnlyFuse(mountpoint), mountpoint, nothreads=True, foreground=True)
    print('\nUmounting mountpoint: \'%s\'...' % (mountpoint))

if __name__ == '__main__':
    if (len(sys.argv) != 2):
        print('Usage: rofuse.py <mountpoint>')
        sys.exit(-1)
    main(sys.argv[1])
