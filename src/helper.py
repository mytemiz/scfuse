'''
Helper functions
'''

import json
import urllib

from config import client_id

# resolve_url must be formatted with search_key and client_id values before use
resolve_url = 'http://api.soundcloud.com/resolve?url=http://soundcloud.com/{search_key}&client_id={client_id}'

def getUserID(username):
    '''
    retrieve user object from username
    
    :returns: user dictionary
    :rtype: dict
    '''
    url = resolve_url.format(search_key=username, client_id=client_id) 
    with urllib.request.urlopen(url) as response:
        html = response.read().decode('utf-8')

    user = json.loads(html)
    return user
