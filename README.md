# scfuse #

FUSE implementation of SoundCloud web service using [Python](http://www.python.org).

### How do I get set up? ###

### Configuration ###

First of all you need a SoundCloud account. If you don't have an account go to [SoundCloud](http://www.soundcloud.com) website and create an account.

**1.** Get the code

```
#!bash

git clone https://mytemiz@bitbucket.org/mytemiz/scfuse.git
```
**2.** Get access token from the following link: http://mytemiz.github.io
Copy the access token into the config.py.

```
#!python

ACCESS_TOKEN = 'your access token'
```

### Dependencies ###

* **[Python3](http://www.python.org)**
* **[soundcloud-python-0.5.0](https://github.com/soundcloud/soundcloud-python)**
* **[fusepy](https://github.com/terencehonles/fusepy)** You do not need to install this package. This is provided in the source code. But you should check the requirements of this module:

```
#!

fusepy requires FUSE 2.6 (or later) and runs on:

    Linux (i386, x86_64, PPC)
    Mac OS X (Intel, PowerPC)
    FreeBSD (i386, amd64)

```

### How to run ###

Create a directory for mounting. And run the main script.

```
#!sh

cd scfuse

# create mount directory
mkdir mountdir

python3 main.py mountdir
```