from src.scapi import SCApi

# TEST: initializing object
try:
    scon = SCApi()
    print("client - success")
except:
    print("client - Connection failed!")
    
def test_followings():
    # TEST: get following list
    followings = scon.followings()
    print (followings)
    return followings

def test_followers():
    # TEST: get following list
    followers = scon.followers()
    print (followers)
    return followers

def test_groups():
    groups = scon.groups()
    print (groups)
    return groups

def test_likes():
    # TEST: get like list
    likes = scon.likes()
    print (likes)
    return likes

def test_playlists():
    # TEST: get playlist list
    playlists = scon.playlists()
    print (playlists)
    return playlists

def test_searchTracks():
    tracks = scon.searchTracks('the intro')
    print (tracks)
    return tracks


def test_tracks():
    tracks = scon.tracks()
    print (tracks)
    return tracks

def test_createPlaylist():
    scon.createPlaylist('My New Set')

def test_followUser(userid):
    scon.followUser(userid)

def test_unfollowUser(userid):
    scon.unfollowUser(userid)

def test_likeTrack(trackid):
    scon.likeTrack(trackid)

def test_unlikeTrack(trackid):
    scon.unlikeTrack(trackid)

def test_uploadAudio(audio, title=None):
    #try: 
    scon.uploadAudio(audio, title)
    #except:
    #    print("UPLOAD FAILED!!")
